package Lab2;

import java.io.*;

/**
 * This class implement a file transfer client through the use of the
 * CommunicationInterface.java interface
 * 
 * @author Michael Kogan and James Wu
 *
 */
public class FTClient {

	/**
	 * The communications always happen on the port set here
	 */
	private static final int SERVER_PORT = 1024;

	/**
	 * The address of the server is set here
	 */
	private static final String SERVER_ADDRESS = "137.94.176.63";

	/**
	 * Object to allow file transfers
	 */
	private TCPFileTransfer myFileTransfer;

	/**
	 * This method runs the client, checks the command and does different things
	 * depending on value of command
	 * 
	 * @throws IOException
	 */
	public void run() throws IOException {
		// Some string variables, one to track user input, one to track tokens,
		// and one regex for delimiters
		String userInput, tokens[], delims = "[ ]*,[ ]*";
		// BufferedReader to receive user input
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		// TCPFileTransfer object to perform file transfers
		myFileTransfer = new TCPFileTransfer();
		// Initialize client in TCPFileTransfer
		myFileTransfer.initializeClient(SERVER_ADDRESS, SERVER_PORT);
		// Infinite loop for program
		while (true) {
			// Prompt user input
			System.out.print("Please enter a command: ");
			// Receive user input
			userInput = br.readLine();
			// Parse user input
			tokens = userInput.split(delims);
			// Interpret user input
			switch (tokens[0]) {
			// Handle get command
			case "get":
				getFile(tokens[1]);
				break;
			// Handle put command
			case "put":
				putFile(tokens[1]);
				break;
			// Handle quit command
			case "quit":
				closeClient();
				break;
			// Invalid input
			default:
				System.out.println("Invalid command");
				break;
			}// switch
		} // while
	}// run()

	/**
	 * This function downloads a file from the server
	 * 
	 * @param fileName
	 *            The name of the file that we are trying to download
	 */
	private void getFile(String fileName) {
		// Initiate download
		System.out.println("Getting: " + fileName);
		// Ask server if file exists
		myFileTransfer.sendCommand("get," + fileName);
		// Get server response
		String fileExists = myFileTransfer.getCommand();
		// Interpret server response
		switch (fileExists) {
		// File exists
		case "Y":
			// Initiate file transfer
			System.out.println("File exists, initiating file transfer");
			// Receive file
			myFileTransfer.receiveFile("./Client/Receive/" + fileName);
			// Transfer complete
			System.out.println("Transfer complete");
			break;
		// File doens't exist
		case "N":
			System.out.println("File doesn't exist on server, please try a different file");
			break;
		// Server error
		default:
			System.out.println("A server error has occured, please try again");
			break;
		}// switch
	}// getFile()

	/**
	 * This function uploads a file
	 * 
	 * @param fileName
	 *            The name of the file that we wish to upload
	 */
	private void putFile(String fileName) {
		// Check if file exists
		if (!(new File("./Client/Send/" + fileName).exists())) {
			System.out.println("File: " + fileName + " doesnt exist!");
			return;
		} // if
			// File exists, inform server of intention to upload
		System.out.println("Sending command: put, " + fileName);
		myFileTransfer.sendCommand("put," + fileName);
		// Initiate upload
		System.out.println("Sending file");
		myFileTransfer.sendFile("./Client/Send/" + fileName);
		// Upload complete
		System.out.println("Transfer complete");
	}// putFile

	/**
	 * This function closes the client
	 */
	private void closeClient() {
		// Inform server of intention to close connection
		myFileTransfer.sendCommand("quit");
		// Close the connection
		myFileTransfer.closeConnection();
		// Exit
		System.out.println("Goodbye");
		System.exit(0);
	}// closeClient()
}// FTClient
