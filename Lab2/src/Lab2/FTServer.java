package Lab2;

import java.io.File;

/**
 * This class implement a file transfer server through the use of the
 * CommunicationInterface.java interface
 * 
 * @author James Wu and Michael Kogan
 *
 */
public class FTServer implements Runnable {

	/**
	 * The communications always happen on the port set here
	 */
	private static final int SERVER_PORT = 1024;

	/**
	 * Strings to keep track of the command and file path
	 */
	String cmd, filePath;

	/**
	 * Object to allow file transfers
	 */
	TCPFileTransfer ftp;

	/**
	 * This method initializes the file transfer server
	 */
	public FTServer() {
		// Set cmd and filePath to null strings, initialize a new
		// TCPFileTransfer object
		cmd = "";
		filePath = "";
		ftp = new TCPFileTransfer();
	}// FTServer()

	/**
	 * This method runs the file transfer server, waiting to receive a command
	 * from the client
	 */
	public void run() {
		// Initialize the server with the given port
		ftp.initializeServer(SERVER_PORT);
		// Infinite while loop for program
		while (true) {
			// Receive command from client
			cmd = ftp.getCommand();
			// Interpret command based on first three letters, since we know
			// that the command is either "put", "get", or "quit"
			switch (cmd.substring(0, 3)) {
			// Handle get command
			case "get":
				// Build the filePath, we know the folder it's in, and the file
				// name is the part of the command after the comma
				filePath = "./Server/Send/" + cmd.substring(cmd.indexOf(",") + 1);
				// Check if the file exists, if it does, tell the client that
				// the file exists and sent the file
				if (new File(filePath).exists()) {
					// Sending 'Y' to client means that the file exists
					ftp.sendCommand("Y");
					// Send file
					ftp.sendFile(filePath);
				} // if
					// File doesn't exist
				else {
					// Sending 'N' to client means file doens't exist
					ftp.sendCommand("N");
				} // else
				break;
			// Handle the put command
			case "put":
				// Build the filePath, we know the folder it's going to be put
				// in, and the file name is the part of the command after the
				// comma
				filePath = "./Server/Receive/" + cmd.substring(cmd.indexOf(",") + 1);
				// Receive the file from the user
				ftp.receiveFile(filePath);
				break;
			// Handle the quit command
			case "qui":
				// Close the connection
				ftp.closeConnection();
				// Close the server
				System.exit(0);
				break;
			// Command from client not valid
			default:
				throw new IllegalArgumentException("Invalid command");
			}// switch
		} // while
	}// run()
}// FTServer
