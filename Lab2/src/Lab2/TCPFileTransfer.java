package Lab2;

import java.io.*;
import java.net.*;
import java.nio.file.*;

/**
 * This class implements the CommunicationInterface.java interface, allowing
 * communication between any two TCPFileTransfer classes
 * 
 * @author s27113
 *
 */
public class TCPFileTransfer implements CommunicationInterface {

	/**
	 * The size of the buffer that we use to transmit files
	 */
	static final int BUFFER_SIZE = 1024;

	/**
	 * The socket of the client, the one we are communication on
	 */
	Socket clientSocket;

	/**
	 * The incoming data stream
	 */
	DataInputStream dis;

	/**
	 * The outgoing data stream
	 */
	DataOutputStream dos;

	@SuppressWarnings("resource")
	@Override
	public void initializeServer(int sourcePort) {
		try {
			// Initialize the client socket
			clientSocket = new ServerSocket(sourcePort).accept();
			// Initialize the incoming data stream
			dis = new DataInputStream(clientSocket.getInputStream());
			// Initialize the outgoing data stream
			dos = new DataOutputStream(clientSocket.getOutputStream());
		} // try
		catch (IOException e) {
			// Printing the stack trace allows us to debug
			e.printStackTrace();
		} // catch
	}// initializeServer()

	@Override
	public void initializeClient(String address, int destinationPort) {
		try {
			// Initialize socket
			clientSocket = new Socket(address, destinationPort);
			// Initialize the incoming data stream
			dis = new DataInputStream(clientSocket.getInputStream());
			// Initialize the outgoing data stream
			dos = new DataOutputStream(clientSocket.getOutputStream());
		} // try
		catch (IOException e) {
			e.printStackTrace();
		} // catch
	}// initializeClient()

	@Override
	public void sendFile(String filePath) {
		// Variable to keep track of file
		File f;
		// Variable to keep track of the file input stream
		FileInputStream in;
		// Variable to keep track of the number of bytes that we've read
		int bytesRead;
		// Buffer used to send a part of the file
		byte[] buffer = new byte[BUFFER_SIZE];
		try {
			// Open the file specified by the filePath
			f = new File(filePath);
			// Create a new file input stream with the file that we just opened
			in = new FileInputStream(f);
			// Send the length of the file to the receiver, receiver must know
			// how many bytes there are in order to avoid the EOS which causes
			// the "socket closed" issue which is encountered in the subsequent
			// file transfer.
			dos.writeLong(f.length());
			// Fill the buffer with as many bytes as we can up to the size of
			// the buffer, the
			// exact number of bytes is returned by the read function, if there
			// are no more bytes in the file, the read function will return -1.
			while ((bytesRead = in.read(buffer)) != -1) {
				// Send the current buffer
				dos.write(buffer, 0, bytesRead);
			}
			// Close the connection
			in.close();
		} // try
		catch (IOException e) {
			// Print stack trace to allow for easy debugging
			e.printStackTrace();
		} // catch
	}// sendFile()

	@SuppressWarnings("resource")
	@Override
	public void receiveFile(String filePath) {
		// Buffer to store chunks of file
		byte[] buffer = new byte[1024];
		// Variable to store the number of bytes that we've read in
		int bytesRead;
		// The size of the file
		long fileSize;
		// The file output stream used to write the file to the incoming folder
		FileOutputStream out;
		try {
			// Open the file output stream to the directory to which we wish to
			// write
			out = new FileOutputStream(filePath);
			// Get the file size from the send function to avoid errors
			fileSize = dis.readLong();
			// While there are still more bytes to receive, keep receiving
			while (fileSize > 0) {
				// Read the bytes from the input stream, but them into the
				// buffer, the read function returns the number of bytes that
				// we've read.
				bytesRead = dis.read(buffer, 0, (int) Math.min(buffer.length, fileSize));
				// Make sure the number of bytes read is not -1, this should
				// never happen, but we are testing just in case.
				if (bytesRead == -1)
					throw new IOException("unexpected EOS");
				// Write the bytes that we've received to the file in the
				// directory that we've specified
				out.write(buffer, 0, bytesRead);
				// Update the file size
				fileSize -= bytesRead;
			} // while
				// Close the output file stream
			out.close();
		} // try
		catch (IOException e) {
			// Print stack trace for debugging
			e.printStackTrace();
		} // catch
	}// receiveFile()

	@Override
	public void sendCommand(String command) {
		try {
			// Send the command to the output stream
			dos.writeUTF(command);
		} // try
		catch (IOException e) {
			// Print stack trace for debug
			e.printStackTrace();
		} // catch
	}// sendCommand()

	@Override
	public void error(String e) {
		// Not implemented
	}// error()

	@Override
	public String getCommand() {
		try {
			// Read the command from the input stream
			return dis.readUTF();
		} // try
		catch (IOException e) {
			// Print stack trace for debug
			e.printStackTrace();
		} // catch
		return null;
	}// getCommand()

	@Override
	public void closeConnection() {
		try {
			//Close the socket
			clientSocket.close();
		}//try 
		catch (IOException e) {
			//Print stack trace for debug
			e.printStackTrace();
		}//catch
	}// closeConnection()

}
